﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Text;
using System.Threading;

namespace Worker
{
    class Worker
    {
        //It will handle messages delivered by RabbitMQ and perform the task
        static void Main(string[] args)
        {
            Console.WriteLine("Worker started.");

            var factory = new ConnectionFactory() { HostName = "localhost" };
            using (var connection = factory.CreateConnection())
            {
                using (var channel = connection.CreateModel())
                {
                    channel.QueueDeclare(queue: "queue",
                        durable: true, //if server dies messages aren't lost
                        exclusive: false,
                        autoDelete: false,
                        arguments: null);

                    var consumer = new EventingBasicConsumer(channel);
                    //We're about to tell the server to deliver us the messages from the queue. 
                    //Since it will push us messages asynchronously, we provide a callback. 
                    //That is what EventingBasicConsumer.Received event handler does.

                    consumer.Received += (model, ea) =>
                    {
                        var body = ea.Body;
                        var message = Encoding.UTF8.GetString(body);
                        Console.WriteLine($"Received: {message}");

                        //Fake job
                        int dots = message.Split('.').Length - 1;
                        Thread.Sleep(dots * 1000);

                        Console.WriteLine("Done.");

                        //Using this code we can be sure that even if you kill a worker using CTRL+C while it was processing a message, 
                        //nothing will be lost. Soon after the worker dies all unacknowledged messages will be redelivered.
                        //Even if the consumer dies, the task isn't lost
                        channel.BasicAck(deliveryTag: ea.DeliveryTag,
                            multiple: false); // we turned off ack and send ack manually
                        
                    };

                    channel.BasicConsume(queue: "queue",
                        //autoAck: true, //automatic acknowledgement mode off
                        autoAck: false,  //send ack manualy
                        consumer: consumer);

                    Console.WriteLine("Press [enter] to exit.");
                    Console.ReadLine();
                }
            }

        }
    }
}
