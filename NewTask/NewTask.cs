﻿using RabbitMQ.Client;
using System;
using System.Text;

namespace NewTask
{
    class NewTask
    {
        static void Main(string[] args)
        {
            Console.WriteLine("New task started [I'm producer/publisher/sender]");

            var factory = new ConnectionFactory() { HostName = "localhost" };
            using (var connection = factory.CreateConnection())
            {
                using (var channel = connection.CreateModel())
                {
                    channel.QueueDeclare(queue: "queue",
                        durable: true, //At this point we're sure that the task_queue queue won't be lost even if RabbitMQ restarts.
                        exclusive: false,
                        autoDelete: false,
                        arguments: null);

                    string message = GetMessage(args);
                    var body = Encoding.UTF8.GetBytes(message);

                    //The persistence guarantees aren't strong, but it's more than enough for our simple task queue.
                    //If you need a stronger guarantee then you can use publisher confirms.
                    var props = channel.CreateBasicProperties();
                    props.Persistent = true; //must be true on message level because we turned on durable on a channel layer 

                    //This tells RabbitMQ not to give more than one message to a worker at a time. 
                    //Or, in other words, don't dispatch a new message to a worker until it has processed and acknowledged the previous one. 
                    //Instead, it will dispatch it to the next worker that is not still busy.
                    channel.BasicQos(0, 1, false);

                    channel.BasicPublish(exchange: "",
                        routingKey: "queue",
                        basicProperties: props,
                        body: body);

                    Console.WriteLine($"Sent {message}");
                }
                Console.WriteLine("Press [enter] to exit.");
                Console.ReadLine();
            }
        }

        private static string GetMessage(string[] args)
        {
            return ((args.Length > 0) ? string.Join(" ", args) : "Hello World!");
        }
    }
}
