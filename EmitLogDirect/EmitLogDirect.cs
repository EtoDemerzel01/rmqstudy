﻿using System;
using System.Linq;
using System.Text;
using RabbitMQ.Client;

namespace EmitLogDirect
{
    class EmitLogDirect
    {
        static void Main(string[] args)
        {
            var factory = new ConnectionFactory() { HostName = "localhost" };
            using (var conn = factory.CreateConnection())
            {
                using (var channel = conn.CreateModel())
                {
                    channel.ExchangeDeclare(exchange: "direct_logs", type: "direct");
                    var severity = (args.Length > 0) ? args[0] : "info";
                    var message = (args.Length > 0) ? string.Join(" ", args.Skip(1).ToArray()) : "Hello World";

                    var body = Encoding.UTF8.GetBytes(message);
                    channel.BasicPublish(exchange: "direct_logs", routingKey: severity, basicProperties: null, body: body);

                    Console.WriteLine($"Sent message '{message}' with {severity} severity.");
                }

                Console.WriteLine("Press [enter] to exit.");
                Console.ReadLine();
            }
        }
    }
}
